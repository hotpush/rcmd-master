from urllib import request
import json
import sys
import time
#import utils
import random
#localip = utils.Utils.get_host_ip()
def post(url, data):
    data = (json.dumps(data))
    req = request.Request(url = url,data = data.encode("utf8"))
    res_data = request.urlopen(req)
    return json.loads(res_data.read().decode("utf8"))

def get_dns():
    rst = post("http://%s:80/get_dns_v1"%localip, {"func": 'access'})
    print(rst)
    return rst

def benchmark():
    stat  = {}
    for i in range(1000):
        rst = get_dns()
        if 'data' in rst:
            for j in rst['data']:
                key = "%s:%d"%(j['ip'], j['port'])
                if key not in stat:
                    stat[key] = 0
                stat[key] += 1
    
    for key in stat:
        print("%s  %d"%(key, stat[key]))


class Node:
    def __init__(self, ip, port, etc):
        self.ip = ip
        self.port = port
        self.status = 0
        self.fail = 0
        self.last = 0
        self.etc = etc
        self.session = None

def select_ips(nodes, module_etc, app_channel, appid):
    ips = []
    limit_ips = None
    app_channel_route = module_etc.get("app_channel_route", {})
    if app_channel_route:
        for route_plan_key in app_channel_route:
            route_plan = app_channel_route[route_plan_key]
            if appid in route_plan.get("appids", []) and app_channel in route_plan.get("app_channels", []):
                limit_ips = set(route_plan.get("ips", []))

    select_range = []
    total_weight = 0
    for i in nodes:
        weight = i.etc.get("weight", 1)
        if i.status == 0 or i.status == 1:
            if not limit_ips or i.ip in limit_ips:
                select_range.append(i)
                total_weight += weight

    IP_COUNT = 2
    for k in range(IP_COUNT):
        choice = random.randint(1, total_weight)
        for i in select_range:
            weight = i.etc.get("weight", 1)
            choice -= weight
            if choice <= 0:
                ips.append(i)
                total_weight -= weight
                select_range.remove(i)
                break
        if not select_range:
            break
    return ips


def test_select_ips():
    module_etcs = [{}, {"app_channel_route": {"prepod_env": {"app_channels": ["xiaomi"], "appids": ["xwzzd"], "ips":["1.1.1.1", "1.1.1.2"]}}}]
    nodes = [
        Node('1.1.1.1', 10, {"weight": 10}),
        Node('1.1.1.2', 10, {"weight": 5}),
        Node('1.1.1.3', 10, {"weight": 20}),
        Node('1.1.1.4', 10, {"weight": 20}),
        Node('1.1.1.5', 10, {"weight": 30}),
        Node('1.1.1.6', 10, {"weight": 15}),
    ]
    app_channels = ["", "xiaomi"]
    for u in range(2):
        app_channel = app_channels[u]
        module_etc = module_etcs[u]
        appid = 'xwzzd'
        ips_stat = [{}, {}]
        total = 0
        for i in range(1000):
            ips = select_ips(nodes, module_etc, app_channel, appid)
            for j in range(len(ips)):
                ip = ips[j].ip
                if ip not in ips_stat[j]:
                    ips_stat[j][ip] = 0
                ips_stat[j][ip] += 1
            total += 1
        for k in ips_stat:
            for ip in k:
                k[ip] /= total * 1.0
        print("%s %s"%(app_channel, module_etc))
        print(ips_stat)

if __name__ == "__main__":
    #benchmark()
    test_select_ips()