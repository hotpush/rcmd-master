# rcmd-master

#### 项目介绍
推荐引擎master

#### API
##### get_dns_v1
拉取DNS
请求:
```
GET /get_dns_v1 HTTP 1/1
{
    "func": 表示拉取接入点路由, 小程序消息流拉取填"stream_sapp", APP端APPLOG日志回传填"applog"
    "ac": 网络类型，字段定义跟TT的一样就行,
    "device_platform": 操作系统平台，跟TT的定义一样,
    "loc_time": 本地当前时间，跟TT定义一样, 
    "latitude": 纬度，跟TT定义一样, 
    "longtitude": 经度,跟TT定义一样,
    "city": 城市, 跟TT定义一样,
    "iid": TT的install_id, 
    "device_id": TT的deviceid, 
    "udid": imei,跟TT定义一样,
    "openudid": 跟TT定义一样, 
    "device_type": 手机型号了，跟TT定义一样, 
    "os_api": 定义一样, 
    "os_version": 操作系统类型，跟TT一样, 
    "client_version": 我们客户端的版本号,传字符串
}
```
回包:
```
{
    'utime': 回包时间戳,
    'seq': 没有用,
    'data': [
                {'type':'ipport', 'ip': '192.168.0.1', 'port': 22980},
                {'type':'ipport', 'ip': '192.168.0.1', 'port': 22981}
            ],
    'error':｛'code':错误码,'message':错误信息｝//如果没有错误error为null
}
```
